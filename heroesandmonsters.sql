-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2019 at 07:37 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `heroesandmonsters`
--

-- --------------------------------------------------------

--
-- Table structure for table `heroes`
--

CREATE TABLE `heroes` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL DEFAULT 'empty',
  `last_name` varchar(30) NOT NULL DEFAULT 'snow' COMMENT 'GOT bastard son',
  `level` tinyint(4) NOT NULL DEFAULT 1,
  `race` varchar(30) NOT NULL DEFAULT 'empty',
  `class` varchar(30) NOT NULL DEFAULT 'empty',
  `weapon` varchar(30) NOT NULL DEFAULT 'empty',
  `st_strength` tinyint(100) NOT NULL DEFAULT 0,
  `st_intelligence` tinyint(100) NOT NULL DEFAULT 0,
  `st_dexterity` varchar(100) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '2019-09-27 00:00:00',
  `modified` datetime NOT NULL DEFAULT '2019-09-27 00:00:00',
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `heroes`
--

INSERT INTO `heroes` (`id`, `name`, `last_name`, `level`, `race`, `class`, `weapon`, `st_strength`, `st_intelligence`, `st_dexterity`, `created`, `modified`, `deleted`) VALUES
(1, 'Connan', 'The Barbarian', 1, 'Human', 'Warrior', 'Bow and Arrows', 100, 30, '80', '2019-09-30 00:57:00', '2019-09-30 00:57:00', '2019-09-30 10:52:45'),
(2, 'He-man', 'grayscol', 1, 'Dwarf', 'Paladin', 'Sword', 1, 1, '1', '2019-09-30 04:30:18', '2019-09-30 04:30:18', '2019-10-01 00:31:25'),
(3, 'Robocop', 'Murphy', 1, 'Elf', 'Paladin', 'Sword', 1, 1, '1', '2019-09-30 05:54:37', '2019-09-30 05:54:37', NULL),
(4, 'Thor', 'Odin', 1, 'Halfling', 'Cleric', 'Sword', 1, 1, '1', '2019-09-30 05:58:49', '2019-09-30 05:58:49', NULL),
(5, 'Loky', 'Odin', 1, 'Elf', 'Wizard', 'Staff', 1, 1, '1', '2019-09-30 06:00:20', '2019-09-30 06:00:20', NULL),
(6, 'Hercules', 'Zeus', 1, 'Dwarf', 'Barbarian', 'Hammer', 1, 1, '1', '2019-09-30 06:01:36', '2019-10-02 04:27:14', NULL),
(7, 'Severus', 'Snape', 1, 'Human', 'Warrior', 'Sword', 100, 30, '80', '2019-09-30 06:02:00', '2019-09-30 06:02:00', NULL),
(8, 'Superman', 'Villa Chica', 1, 'Elf', 'Wizard', 'Staff', 1, 1, '1', '2019-09-30 23:37:06', '2019-09-30 23:37:06', '2019-10-01 00:33:03'),
(9, 'Gandalf', 'Comarca', 1, 'Elf', 'Wizard', 'Staff', 1, 1, '1', '2019-09-30 23:37:36', '2019-09-30 23:37:36', NULL),
(10, 'Harry', 'Potter', 1, 'Elf', 'Wizard', 'Staff', 1, 1, '1', '2019-09-30 23:44:40', '2019-09-30 23:44:40', NULL),
(11, 'Merlyn', '', 1, 'Halfling', 'Wizard', 'Staff', 1, 1, '1', '2019-09-30 23:47:15', '2019-09-30 23:47:15', NULL),
(12, 'Mago Oscuro', '', 1, 'Halfling', 'Wizard', 'Staff', 1, 1, '1', '2019-09-30 23:47:49', '2019-09-30 23:47:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `monsters`
--

CREATE TABLE `monsters` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL DEFAULT 'empty',
  `last_name` varchar(30) NOT NULL DEFAULT 'snow' COMMENT 'GOT bastard son',
  `level` tinyint(4) NOT NULL DEFAULT 1,
  `race` varchar(30) NOT NULL DEFAULT 'empty',
  `power` varchar(30) NOT NULL DEFAULT 'empty',
  `st_strength` tinyint(100) NOT NULL DEFAULT 0,
  `st_intelligence` tinyint(100) NOT NULL DEFAULT 0,
  `st_dexterity` varchar(100) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '2019-09-27 00:00:00',
  `modified` datetime NOT NULL DEFAULT '2019-09-27 00:00:00',
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `monsters`
--

INSERT INTO `monsters` (`id`, `name`, `last_name`, `level`, `race`, `power`, `st_strength`, `st_intelligence`, `st_dexterity`, `created`, `modified`, `deleted`) VALUES
(1, 'Godzilla', 'grayscol', 1, 'Beholder', 'Crunch', 1, 1, '1', '2019-10-02 05:20:33', '2019-10-02 05:48:54', NULL),
(2, 'Khal', 'Args', 1, 'Beholder', 'Shadow Ball', 1, 1, '1', '2019-10-02 05:54:09', '2019-10-02 06:11:06', '2019-10-02 06:58:40'),
(3, 'Cell', 'Z', 1, 'Beholder', 'Shadow Ball', 1, 1, '1', '2019-10-02 06:53:16', '2019-10-02 06:53:16', NULL),
(4, 'Freezer', 'Cooler', 1, 'Stone Giant', 'Crunch', 1, 1, '1', '2019-10-02 06:57:47', '2019-10-02 06:57:47', NULL),
(5, 'Gala', 'Earth', 1, '\r\nDisplacer Beast', 'Crunch', 1, 1, '1', '2019-10-02 06:58:11', '2019-10-02 06:58:11', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `heroes`
--
ALTER TABLE `heroes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monsters`
--
ALTER TABLE `monsters`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `heroes`
--
ALTER TABLE `heroes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `monsters`
--
ALTER TABLE `monsters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
