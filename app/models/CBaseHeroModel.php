<?php

use SlimFacades\Database;

class CBaseHeroModel
{

    protected $name = "test";
    protected $lastname = "test";
    protected $class = "test";
    protected $race = "test";
    protected $weapon = 'test';
    protected $st_strength;
    protected $st_intelligence;
    protected $st_dexterity;

    private $c;

    public function __construct($container)
    {
        $this->c = $container;
        Database::connectTo('default');
    }

    // protected function validateParams()
    // {

    //     if (!empty(intval(preg_replace('/[^0-9]+/', '', $this->name)))) {
    //         return false;
    //     }

    //     if (!empty(intval(preg_replace('/[^0-9]+/', '', $this->lastname)))) {
    //         return false;
    //     }

    //     if (!empty(intval(preg_replace('/[^0-9]+/', '', $this->class)))) {
    //         return false;
    //     }

    //     if (!empty(intval(preg_replace('/[^0-9]+/', '', $this->race)))) {
    //         return false;
    //     }

    //     if (!empty(intval(preg_replace('/[^0-9]+/', '', $this->weapon)))) {
    //         return false;
    //     }

    //     return true;
    // }

//////////////////////////////////////////////////////////////////////////////
// START SECTION: CRUD
//////////////////////////////////////////////////////////////////////////////
    //CREATE
    public function create($params = [])
    {
        try{
            $insertData = [
                'id' => '',
                'name' => $params['name'],
                'last_name' => $params['last_name'],
                'level' => '1',
                'race' => $params['race'],
                'class' => $params['class'],
                'weapon' => $params['weapon'],
                'st_strength'=> empty($params['st_strength']) ? 1 : $params['st_strength'],
                'st_intelligence'=> empty($params['st_intelligence']) ? 1 : $params['st_intelligence'],
                'st_dexterity'=> empty($params['st_dexterity']) ? 1 : $params['st_dexterity'],
                'created'=> date('Y-m-d H:i:s'),
                'modified'=> date('Y-m-d H:i:s'),
                'deleted'=> null,
            ];
            $insert = Database::table('heroes')
                ->insertGetId($insertData);
            
                die($insert);

            } catch (Exception $e) {
                return [];
            }
    }
    //UPDATE
    public function update($params = [])
    {          

            $update = Database::table('heroes')
            ->where('id', '=', $params['id'])
            ->update([
                'name' => $params['name'],
                'last_name' => $params['last_name'],
                'race' => $params['race'],
                'class' => $params['class'],
                'weapon' => $params['weapon'],
                'st_strength'=> empty($params['st_strength']) ? 1 : $params['st_strength'],
                'st_intelligence'=> empty($params['st_intelligence']) ? 1 : $params['st_intelligence'],
                'st_dexterity'=> empty($params['st_dexterity']) ? 1 : $params['st_dexterity'],
                'modified'=> date('Y-m-d H:i:s'),
            ]);
            return $update;
    }
    //DELETE
    public function deleteById($param)
    {
        $url = $_SERVER['SCRIPT_NAME'];
        $update = Database::table('heroes')
        ->where('id', '=', $param)
        ->update([
            'deleted' => date('Y-m-d H:i:s')
        ]);
        echo 'ok'; 
        return $url;
    }

//////////////////////////////////////////////////////////////////////////////
// END SECTION: CRUD
//////////////////////////////////////////////////////////////////////////////

    public function popular($column = "",$array = [])
    {   
        $popular="";
        foreach($array as $key => $val) {
            $data =  Database::table('heroes')->where($column, $key);
            $array[$key] = $data->count();
        }
        $max = 0;
        foreach($array as $key => $val) {
	        if($val> $max){
		        $max=$val;
		        $popular=$key;
	        }
        }
        return $popular;
    }

    public function getAll()
    {
        $results = Database::table('heroes')->where('deleted', '=', null)->get();
        return $results;
    }

    public function getHeroById($param)
    {
        try {
            if (!isset($param))
                throw new Exception('id parameter is empty.');

            $profileDetails = Database::table('heroes')
                ->where('id', '=', $param)
                ->get();
                // die(var_dump((array)$profileDetails));
            return $profileDetails;

        } catch (Exception $e) {
            return [];
        }
    }

    public function countAll()
    {   
        $races=['Human'=> 0, 'Elf'=> 0, 'Halfling'=> 0, 'Dwarf'=> 0, 'Half-orc'=> 0, 'Half-elf'=> 0, 'Dragonborn'=> 0];
        $classes=['Paladin'=> 0, 'Ranger'=> 0, 'Barbarian'=> 0, 'Wizard'=> 0, 'Cleric'=> 0, 'Warrior'=> 0, 'Thief'=> 0];
        $weapons=['Sword'=> 0, 'Dagger'=> 0, 'Hammer'=> 0, 'Bow and Arrows'=> 0, 'Staff'=> 0];
        $heros = Database::table('heroes')->where('deleted', '=', null)->get();
        
        $data=[
            'totalheroes'=> $heros->count(), 
            'race'=> $this->popular('race',$races),
            'class'=> $this->popular('class',$classes),
            'weapon'=> $this->popular('weapon',$weapons),
        ];

        return $data;
    }
}