<?php

use SlimFacades\Database;

class CBaseMonsterModel
{

    protected $name = "test";
    protected $lastname = "test";
    protected $class = "test";
    protected $race = "test";
    protected $weapon = 'test';
    protected $st_strength;
    protected $st_intelligence;
    protected $st_dexterity;

    private $c;

    public function __construct($container)
    {
        $this->c = $container;
        Database::connectTo('default');
    }

//////////////////////////////////////////////////////////////////////////////
// START SECTION: CRUD
//////////////////////////////////////////////////////////////////////////////
    //CREATE
    public function create($params = [])
    {
        try{
            $insertData = [
                'id' => '',
                'name' => $params['name'],
                'last_name' => $params['last_name'],
                'level' => '1',
                'race' => $params['race'],
                'power' => $params['power'],
                'st_strength'=> empty($params['st_strength']) ? 1 : $params['st_strength'],
                'st_intelligence'=> empty($params['st_intelligence']) ? 1 : $params['st_intelligence'],
                'st_dexterity'=> empty($params['st_dexterity']) ? 1 : $params['st_dexterity'],
                'created'=> date('Y-m-d H:i:s'),
                'modified'=> date('Y-m-d H:i:s'),
                'deleted'=> null,
            ];
            $insert = Database::table('monsters')
                ->insertGetId($insertData);
            
                die($insert);

            } catch (Exception $e) {
                return [];
            }
    }
    //UPDATE
    public function update($params = [])
    {          

            $update = Database::table('monsters')
            ->where('id', '=', $params['id'])
            ->update([
                'name' => $params['name'],
                'last_name' => $params['last_name'],
                'race' => $params['race'],
                'power' => $params['power'],
                'st_strength'=> empty($params['st_strength']) ? 1 : $params['st_strength'],
                'st_intelligence'=> empty($params['st_intelligence']) ? 1 : $params['st_intelligence'],
                'st_dexterity'=> empty($params['st_dexterity']) ? 1 : $params['st_dexterity'],
                'modified'=> date('Y-m-d H:i:s'),
            ]);
            return $update;
    }
    //DELETE
    public function deleteById($param)
    {
        $url = $_SERVER['SCRIPT_NAME'];
        $update = Database::table('monsters')
        ->where('id', '=', $param)
        ->update([
            'deleted' => date('Y-m-d H:i:s')
        ]);
        echo 'ok'; 
        return $url;
    }

//////////////////////////////////////////////////////////////////////////////
// END SECTION: CRUD
//////////////////////////////////////////////////////////////////////////////

    public function popular($column = "",$array = [])
    {   
        $popular="";
        foreach($array as $key => $val) {
            $data =  Database::table('monsters')->where($column, $key);
            $array[$key] = $data->count();
        }
        $max = 0;
        foreach($array as $key => $val) {
	        if($val> $max){
		        $max=$val;
		        $popular=$key;
	        }
        }
        return $popular;
    }

    public function getAll()
    {
        $results = Database::table('monsters')->where('deleted', '=', null)->get();
        return $results;
    }

    public function getMonsterById($param)
    {
        try {
            if (!isset($param))
                throw new Exception('id parameter is empty.');

            $profileDetails = Database::table('monsters')
                ->where('id', '=', $param)
                ->get();
                // die(var_dump((array)$profileDetails));
            return $profileDetails;

        } catch (Exception $e) {
            return [];
        }
    }

    public function countAll()
    {   
        $races=["Beholder"=> 0,"Mind Flayer"=> 0,"Drow"=> 0,"Dragons"=> 0,"Owlbear"=> 0,"Bulette"=> 0,"Rust Monster"=> 0,"Gelatinous
        Cube"=> 0,"Hill Giant"=> 0,"Stone Giant"=> 0,"Frost Giant"=> 0,"Fire Giant"=> 0,"Cloud Giant"=> 0,"Storm Giant"=> 0,"
        Displacer Beast"=> 0,"Githyanki"=> 0,"Kobold"=> 0,"Kuo-Toa"=> 0,"Lich"=> 0,"Orc"=> 0,"Slaad"=> 0,"Umber Hulk"=> 0,"Yuan-ti"=> 0];
        $power=["Shadow Ball"=> 0,"Aerial Ace"=> 0,"Giga Drain"=> 0,"Thunderbolt"=> 0,"Earthquake"=> 0,"Crunch"=> 0,"Double Team"=> 0,"
        Psychic"=> 0,"Ice Beam"=> 0,"Surf"=> 0];
        $monster = Database::table('monsters')->where('deleted', '=', null)->get();
        
        $data=[
            'totalmonsters'=> $monster->count(), 
            'race'=> $this->popular('race',$races),
            'power'=> $this->popular('power',$power),
        ];

        return $data;
    }
}