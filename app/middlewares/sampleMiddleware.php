<?php

    Class sampleMiddleware {

        public function __invoke($request, $response, $next) {
            $response->getBody()->write('ANTES');
            $response = $next($request, $response);
            $response->getBody()->write('DESPUES');

            return $response;
        }
    }

/* path: ~app/middlewares/sampleMiddleware.php */