<?php
/* Smarty version 3.1.33, created on 2019-10-02 05:48:50
  from 'C:\xampp\htdocs\sraxheromonster\app\views\monsters\modifyMonster.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d941e22044d30_40602195',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8a19b0fff9acd411bf31ab9e2781dd356304d8db' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sraxheromonster\\app\\views\\monsters\\modifyMonster.tpl',
      1 => 1569988126,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d941e22044d30_40602195 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\xampp\\htdocs\\sraxheromonster\\app\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.replace.php','function'=>'smarty_modifier_replace',),));
$_smarty_tpl->_assignInScope('racearray', array("Beholder","Mind Flayer","Drow","Dragons","Owlbear","Bulette","Rust Monster","Gelatinous
Cube","Hill Giant","Stone Giant","Frost Giant","Fire Giant","Cloud Giant","Storm Giant","
Displacer Beast","Githyanki","Kobold","Kuo-Toa","Lich","Orc","Slaad","Umber Hulk","Yuan-ti"));
$_smarty_tpl->_assignInScope('powerarray', array("Shadow Ball","Aerial Ace","Giga Drain","Thunderbolt","Earthquake","Crunch","Double Team","
Psychic","Ice Beam","Surf"));?>

<?php $_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1>Monster Modify</h1>
</div>

<form name="formModifyMonster" id="formModifyMonster" action=<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['SCRIPT_NAME']->value,'index.php','updatemonster');?>
 method="post">
  <div class="form-group">
    <label for="newHeroFirstName">Name Hero</label>
    <input type="text" class="form-control" id="newHeroFirstName" name="name" required value=<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
>
  </div>
  <div class="form-group">
    <label for="newHeroLastName">Last Name</label>
    <input type="text" class="form-control" id="newHeroLastName" name="last_name" value=<?php echo $_smarty_tpl->tpl_vars['last_name']->value;?>
>
  </div>
  <div class="form-group">
    <label for="selectRace">Select Race <?php echo $_smarty_tpl->tpl_vars['race']->value;?>
</label>
    <select class="form-control" id="selectRace" name="race" value=<?php echo $_smarty_tpl->tpl_vars['race']->value;?>
>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['racearray']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
    <?php ob_start();
echo $_smarty_tpl->tpl_vars['race']->value;
$_prefixVariable1 = ob_get_clean();
if ($_smarty_tpl->tpl_vars['key']->value == $_prefixVariable1) {?>
	    <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
 selected><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php } else { ?>
      <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
  </div>
    <div class="form-group">
    <label for="selectPower">Select Power <?php echo $_smarty_tpl->tpl_vars['power']->value;?>
</label>
    <select class="form-control" id="selectPower" name="power" value=<?php echo $_smarty_tpl->tpl_vars['race']->value;?>
>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['powerarray']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
    <?php ob_start();
echo $_smarty_tpl->tpl_vars['power']->value;
$_prefixVariable2 = ob_get_clean();
if ($_smarty_tpl->tpl_vars['key']->value == $_prefixVariable2) {?>
	    <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
 selected><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php } else { ?>
      <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
  </div>
  <div class="form-group d-none">
    <label for="newHeroStrength">St Strength</label>
    <input type="text" class="form-control" id="newHeroStrength" name="st_strength ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroIntelligence">St Intelligence</label>
    <input type="text" class="form-control" id="newHeroIntelligence" name="st_intelligence ">
  </div>
  <div class="form-group d-none">
    <label for="newHeroDexterity">St Dexterity</label>
    <input type="text" class="form-control" id="newHeroDexterity" name="st_dexterity">
  </div>
  <div class="form-group d-none">
    <label for="modifyMonsterId">ID</label>
    <input type="text" class="form-control" id="modifyMonsterId" name="id" value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
>
  </div>
  <button id="btnSubmitModifyMonster" type="button" class="btn btn-primary">Submit</button>
</form>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
