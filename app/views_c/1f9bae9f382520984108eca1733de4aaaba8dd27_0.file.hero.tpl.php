<?php
/* Smarty version 3.1.33, created on 2019-09-30 07:35:06
  from 'C:\xampp\htdocs\sraxheromonster\app\views\heroes\hero.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d91940a5ba908_86007524',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1f9bae9f382520984108eca1733de4aaaba8dd27' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sraxheromonster\\app\\views\\heroes\\hero.tpl',
      1 => 1569821704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d91940a5ba908_86007524 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="card" style="width:250px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "heroes.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Create Hero</h5>
        <a href="newhero" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
    <div class="col-sm-6">
    <div class="card" style="width:250px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "edithero.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Edit Hero & Delete</h5>
        <a href="edithero" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
