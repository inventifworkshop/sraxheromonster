<?php
/* Smarty version 3.1.33, created on 2019-10-02 06:49:25
  from 'C:\xampp\htdocs\sraxheromonster\app\views\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d942c551768a3_96560339',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a89a74d87e877ebed8b30d8fedf12dcc33bcaafc' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sraxheromonster\\app\\views\\index.tpl',
      1 => 1569991760,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d942c551768a3_96560339 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
</div>
<div class="row">
  <div class="col-sm-3">
    <div class="card" style="width:200px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "heroes.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Total Available Heroes</h5>
        <h3><?php echo $_smarty_tpl->tpl_vars['datah']->value['totalheroes'];?>
</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card" style="width:200px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "popularhero.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Hero Race</h5>
        <h3><?php echo $_smarty_tpl->tpl_vars['datah']->value['race'];?>
</h3>
      </div>
    </div>
  </div>
    <div class="col-sm-3">
    <div class="card" style="width:200px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "popularclass.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Hero Class</h5>
        <h3><?php echo $_smarty_tpl->tpl_vars['datah']->value['class'];?>
</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card" style="width:200px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "popularweapon.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Weapon</h5>
        <h3><?php echo $_smarty_tpl->tpl_vars['datah']->value['weapon'];?>
</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card" style="width:200px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "monsters.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Total Available Monsters</h5>
        <h3><?php echo $_smarty_tpl->tpl_vars['datam']->value['totalmonsters'];?>
</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card" style="width:200px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "popularrace.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Race</h5>
        <h3><?php echo $_smarty_tpl->tpl_vars['datam']->value['race'];?>
</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card" style="width:200px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "popularability.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Ability</h5>
        <h3><?php echo $_smarty_tpl->tpl_vars['datam']->value['power'];?>
</h3>
      </div>
    </div>
  </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
