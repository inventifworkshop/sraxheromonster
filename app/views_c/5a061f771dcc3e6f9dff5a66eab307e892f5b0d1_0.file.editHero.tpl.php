<?php
/* Smarty version 3.1.33, created on 2019-09-30 10:50:46
  from 'C:\xampp\htdocs\sraxheromonster\app\views\heroes\editHero.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d91c1e66a9df7_73669745',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5a061f771dcc3e6f9dff5a66eab307e892f5b0d1' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sraxheromonster\\app\\views\\heroes\\editHero.tpl',
      1 => 1569833443,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d91c1e66a9df7_73669745 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1>Hero Edit</h1>
</div>
<div class="row">
<div class="col-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Number</th>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Race</th>
            <th scope="col">Class</th>
            <th scope="col">Weapon</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['heroes']->value, 'heroe', false, NULL, 'i', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['heroe']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['iteration']++;
?>
          <tr>
            <th scope="row"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['iteration'] : null);?>
</th>
            <td><?php echo $_smarty_tpl->tpl_vars['heroe']->value['name'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['heroe']->value['last_name'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['heroe']->value['race'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['heroe']->value['class'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['heroe']->value['weapon'];?>
</td>
            <td>
              <button type="button" class="btn btn-success"><a style="color: white;" href="modifyhero?id=<?php echo $_smarty_tpl->tpl_vars['heroe']->value['id'];?>
">Update</a></button>
            <button type="button" class="btn btn-danger"><a style="color: white;" href="deletehero?id=<?php echo $_smarty_tpl->tpl_vars['heroe']->value['id'];?>
">Delete</a></button>
            </td>
          </tr>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody>
      </table>
    </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
