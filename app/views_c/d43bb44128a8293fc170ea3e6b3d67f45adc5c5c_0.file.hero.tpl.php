<?php
/* Smarty version 3.1.33, created on 2019-09-28 12:22:04
  from 'C:\xampp\htdocs\apirest\app\views\heroes\hero.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d8f344c047667_72951917',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd43bb44128a8293fc170ea3e6b3d67f45adc5c5c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\apirest\\app\\views\\heroes\\hero.tpl',
      1 => 1569666121,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d8f344c047667_72951917 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
</div>
<div class="row">
  <div class="col-sm-4">
    <div class="card" style="width:200px">
    <img src="http://localhost/apirest/public/assets/template/images/card_image.png" class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Create Hero</h5>
        <a href="#" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
    <div class="col-sm-4">
    <div class="card" style="width:200px">
    <img src="http://localhost/apirest/public/assets/template/images/card_image.png" class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Edit Hero</h5>
        <a href="#" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
    <div class="col-sm-4">
    <div class="card" style="width:200px">
    <img src="http://localhost/apirest/public/assets/template/images/card_image.png" class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Delete Hero</h5>
        <a href="#" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
