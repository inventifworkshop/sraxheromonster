<?php
/* Smarty version 3.1.33, created on 2019-10-02 04:22:20
  from 'C:\xampp\htdocs\sraxheromonster\app\views\heroes\modifyHero.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d9409dc8c18b0_42330697',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7ea84a790d9b68ad517263817babb18817a7315e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sraxheromonster\\app\\views\\heroes\\modifyHero.tpl',
      1 => 1569982936,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d9409dc8c18b0_42330697 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\xampp\\htdocs\\sraxheromonster\\app\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.replace.php','function'=>'smarty_modifier_replace',),));
$_smarty_tpl->_assignInScope('racecompare', array("Human","Elf","Halfling","Dwarf","Half-orc","Half-elf","Dragonborn"));
$_smarty_tpl->_assignInScope('classcompare', array("Paladin","Ranger","Barbarian","Wizard","Cleric","Warrior","Thief"));
$_smarty_tpl->_assignInScope('weaponcompare', array("Sword","Dagger","Hammer","Bow and Arrows","Staff"));
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1>Hero Modify</h1>
</div>

<form name="formModifyHero" id="formModifyHero" action=<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['SCRIPT_NAME']->value,'index.php','updatehero');?>
 method="post">
  <div class="form-group">
    <label for="newHeroFirstName">Name Hero</label>
    <input type="text" class="form-control" id="newHeroFirstName" name="name" required value=<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
>
  </div>
  <div class="form-group">
    <label for="newHeroLastName">Last Name</label>
    <input type="text" class="form-control" id="newHeroLastName" name="last_name" value=<?php echo $_smarty_tpl->tpl_vars['last_name']->value;?>
>
  </div>
  <div class="form-group">
    <label for="selectRace">Select Race <?php echo $_smarty_tpl->tpl_vars['race']->value;?>
</label>
    <select class="form-control" id="selectRace" name="race" value=<?php echo $_smarty_tpl->tpl_vars['race']->value;?>
>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['racecompare']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
    <?php ob_start();
echo $_smarty_tpl->tpl_vars['race']->value;
$_prefixVariable1 = ob_get_clean();
if ($_smarty_tpl->tpl_vars['key']->value == $_prefixVariable1) {?>
	    <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
 selected><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php } else { ?>
      <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
  </div>
  <div class="form-group">
    <label for="selectClass">Select Class</label>
    <select class="form-control" id="selectClass" name="class">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classcompare']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
    <?php ob_start();
echo $_smarty_tpl->tpl_vars['class']->value;
$_prefixVariable2 = ob_get_clean();
if ($_smarty_tpl->tpl_vars['key']->value == $_prefixVariable2) {?>
	    <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
 selected><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php } else { ?>
      <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
    </select>
  </div>
  <div class="form-group">
    <label for="selectWeapon">Select Weapon</label>
    <select class="form-control" id="selectWeapon" name="weapon">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['weaponcompare']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
    <?php ob_start();
echo $_smarty_tpl->tpl_vars['weapon']->value;
$_prefixVariable3 = ob_get_clean();
if ($_smarty_tpl->tpl_vars['key']->value == $_prefixVariable3) {?>
	    <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
 selected><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php } else { ?>
      <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
    <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
  </div>
  <div class="form-group d-none">
    <label for="newHeroStrength">St Strength</label>
    <input type="text" class="form-control" id="newHeroStrength" name="st_strength ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroIntelligence">St Intelligence</label>
    <input type="text" class="form-control" id="newHeroIntelligence" name="st_intelligence ">
  </div>
  <div class="form-group d-none">
    <label for="newHeroDexterity">St Dexterity</label>
    <input type="text" class="form-control" id="newHeroDexterity" name="st_dexterity">
  </div>
  <div class="form-group d-none">
    <label for="modifyHeroId">ID</label>
    <input type="text" class="form-control" id="modifyHeroDexterity" name="id" value=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
>
  </div>
  <button id="btnSubmitModifyHero" type="button" class="btn btn-primary">Submit</button>
</form>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
