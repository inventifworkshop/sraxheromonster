<?php
/* Smarty version 3.1.33, created on 2019-09-30 23:35:58
  from 'C:\xampp\htdocs\sraxheromonster\app\views\heroes\newHero.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d92753ed98c29_94573542',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '22e745717dd68bbfff646a5d21c228bdd6ade894' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sraxheromonster\\app\\views\\heroes\\newHero.tpl',
      1 => 1569879331,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d92753ed98c29_94573542 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\xampp\\htdocs\\sraxheromonster\\app\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.replace.php','function'=>'smarty_modifier_replace',),));
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
</div>
<form name="formNewHero" id="formNewHero" action=<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['SCRIPT_NAME']->value,'index.php','createhero');?>
 method="post">
  <div class="form-group">
    <label for="newHeroFirstName">Name Hero</label>
    <input type="text" class="form-control" id="newHeroFirstName" name="name" required>
  </div>
  <div class="form-group">
    <label for="newHeroLastName">Last Name</label>
    <input type="text" class="form-control" id="newHeroLastName" name="last_name">
  </div>
  <div class="form-group">
    <label for="selectRace">Select Race</label>
    <select class="form-control" id="selectRace" name="race">
      <option value="Human">Human</option>
      <option value="Elf">Elf</option>
      <option value="Halfling">Halfling</option>
      <option value="Dwarf">Dwarf</option>
      <option value="Half-orc">Half-orc</option>
      <option value="Half-elf">Half-elf</option>
      <option value="Dragonborn">Dragonborn</option>
    </select>
  </div>
  <div class="form-group">
    <label for="selectClass">Select Class</label>
    <select class="form-control" id="selectClass" name="class">
      <option value="Paladin">Paladin</option>
      <option value="Ranger" disabled>Ranger</option>
      <option value="Barbarian" disabled>Barbarian</option>
      <option value="Wizard" disabled>Wizard</option>
      <option value="Cleric"disabled>Cleric</option>
      <option value="Warrior" disabled>Warrior</option>
      <option value="Thief" disabled>Thief</option>
    </select>
  </div>
  <div class="form-group">
    <label for="selectWeapon">Select Weapon</label>
    <select class="form-control" id="selectWeapon" name="weapon">
      <option value="Sword">Sword</option>
      <option value="Dagger">Dagger</option>
      <option value="Hammer" disabled>Hammer</option>
      <option value="Bow and Arrows" disabled>Bow and Arrows</option>
      <option value="Staff" disabled>Staff</option>
    </select>
  </div>
  <div class="form-group d-none">
    <label for="newHeroStrength">St Strength</label>
    <input type="text" class="form-control" id="newHeroStrength" name="st_strength ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroIntelligence">St Intelligence</label>
    <input type="text" class="form-control" id="newHeroIntelligence" name="st_intelligence ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroDexterity">St Dexterity</label>
    <input type="text" class="form-control" id="newHeroDexterity" name="st_dexterity">
  </div>
  <button id="btnSubmitNewHero" type="button" class="btn btn-primary">Submit</button>
  <button id="newHeroButtonRandomize" class="btn btn-primary">Randomize</button>
  <button id="newHeroButtonClearForm" class="btn btn-primary">Clear form</button>
</form>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
