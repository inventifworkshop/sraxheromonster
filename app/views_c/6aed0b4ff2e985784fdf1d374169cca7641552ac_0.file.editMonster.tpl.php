<?php
/* Smarty version 3.1.33, created on 2019-10-02 05:29:36
  from 'C:\xampp\htdocs\sraxheromonster\app\views\monsters\editMonster.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d9419a0353d89_39342957',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6aed0b4ff2e985784fdf1d374169cca7641552ac' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sraxheromonster\\app\\views\\monsters\\editMonster.tpl',
      1 => 1569986967,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d9419a0353d89_39342957 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1>Monster Edit</h1>
</div>
<div class="row">
<div class="col-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Number</th>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Race</th>
            <th scope="col">Power</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['monsters']->value, 'monster', false, NULL, 'i', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['monster']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['iteration']++;
?>
          <tr>
            <th scope="row"><?php echo (isset($_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_i']->value['iteration'] : null);?>
</th>
            <td><?php echo $_smarty_tpl->tpl_vars['monster']->value['name'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['monster']->value['last_name'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['monster']->value['race'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['monster']->value['power'];?>
</td>
            <td>
              <button type="button" class="btn btn-success"><a style="color: white;" href="modifymonster?id=<?php echo $_smarty_tpl->tpl_vars['monster']->value['id'];?>
">Update</a></button>
            <button type="button" class="btn btn-danger"><a style="color: white;" href="deletemonster?id=<?php echo $_smarty_tpl->tpl_vars['monster']->value['id'];?>
">Delete</a></button>
            </td>
          </tr>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody>
      </table>
    </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
