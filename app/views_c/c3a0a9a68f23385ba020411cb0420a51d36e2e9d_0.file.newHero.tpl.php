<?php
/* Smarty version 3.1.33, created on 2019-09-28 22:08:02
  from 'C:\xampp\htdocs\apirest\app\views\heroes\newHero.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d8fbda20d33b8_03685329',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c3a0a9a68f23385ba020411cb0420a51d36e2e9d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\apirest\\app\\views\\heroes\\newHero.tpl',
      1 => 1569701278,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d8fbda20d33b8_03685329 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
</div>
<form>
  <div class="form-group">
    <label for="selectNameHero">Name Hero</label>
    <select class="form-control" id="selectNameHero" name="name">
      <option value="Bheizer">Bheizer</option>
      <option value="Khazun">Khazun</option>
      <option value="Grirgel">Grirgel</option>
      <option value="Murgil">Murgil</option>
      <option value="Edraf">Edraf</option>
      <option value="En">En</option>
      <option value="Grognur">Grognur</option>
      <option value="Grum">Grum</option>
      <option value="Surhathion">Surhathion</option>
      <option value="Lamos">Lamos</option>
      <option value="Melmedjad">Melmedjad</option>
      <option value="Shouthes">Shouthes</option>
      <option value="Che">Che</option>
      <option value="Jun">Jun</option>
      <option value="Rircurtun">Rircurtun</option>
      <option value="Zelen">Zelen</option>
    </select>
  </div>
  <div class="form-group">
    <label for="selectLastNameHero">Last Name</label>
    <select class="form-control" id="selectLastNameHero" name="lastname">
      <option value="-">-</option>
      <option value="Nema">Nema</option>
      <option value="Dhusher">Dhusher</option>
      <option value="Burningsun">Burningsun</option>
      <option value="Hawkglow">Hawkglow</option>
      <option value="Nav">Nav</option>
      <option value="Kadev">Kadev</option>
      <option value="Lightkeeper">Lightkeeper</option>
      <option value="Heartdancer">Heartdancer</option>
      <option value="Fivrithrit">Fivrithrit</option>
      <option value="Suechit">Suechit</option>
      <option value="Tuldethatvo">Tuldethatvo</option>
      <option value="Vrovakya">Vrovakya</option>
      <option value="Hiao">Hiao</option>
      <option value="Chiay">Chiay</option>
      <option value="Hogoscu">Hogoscu</option>
      <option value="Vedrimor">Vedrimor</option>
    </select>
  </div>
  <div class="form-group">
    <label for="selectClass">Select Class</label>
    <select class="form-control" id="selectClass" name="class">
      <option value="Paladin">Paladin</option>
      <option value="Range">Range</option>
      <option value="Barbarian">Barbarian</option>
      <option value="Wizard">Wizard</option>
      <option value="Cleric">Cleric</option>
      <option value="Warrior">Warrior</option>
      <option value="Thief">Thief</option>
    </select>
  </div>
  <div class="form-group">
    <label for="selectRace">Select Race</label>
    <select class="form-control" id="selectRace" name="race">
      <option value="Human">Human</option>
      <option value="Elf">Elf</option>
      <option value="Halfling">Halfling</option>
      <option value="Dwarf">Dwarf</option>
      <option value="Half-orc">Half-orc</option>
      <option value="Half-elf">Half-elf</option>
      <option value="Dragonborn">Dragonborn</option>
    </select>
  </div>

  <div class="form-group">
    <label for="selectWeapon">Select Weapon</label>
    <select class="form-control" id="selectWeapon">
      <option>Sword</option>
      <option>Dagger</option>
      <option>Hammer</option>
      <option>Bow and Arrows</option>
      <option>Half-orc</option>
      <option>Half-elf</option>
      <option>Staff</option>
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  <button type="subutton" class="btn btn-primary">Randomize</button>
</form>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
