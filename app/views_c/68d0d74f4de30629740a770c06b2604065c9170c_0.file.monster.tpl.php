<?php
/* Smarty version 3.1.33, created on 2019-10-02 07:06:35
  from 'C:\xampp\htdocs\sraxheromonster\app\views\monsters\monster.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d94305b83e837_41054067',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '68d0d74f4de30629740a770c06b2604065c9170c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\sraxheromonster\\app\\views\\monsters\\monster.tpl',
      1 => 1569992790,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d94305b83e837_41054067 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
</div>
<div class="row">
  <div class="col-sm-4">
    <div class="card" style="width:250px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "monsters.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Create Monster</h5>
        <a href="newmonster" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
    <div class="col-sm-4">
    <div class="card" style="width:250px">
    <img src=<?php echo @constant('DIR_IMAGES');
echo "editmonster.png";?>
 class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Edit Monster</h5>
        <a href="editmonster" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
</div>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
