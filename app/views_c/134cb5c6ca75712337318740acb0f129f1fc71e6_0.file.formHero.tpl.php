<?php
/* Smarty version 3.1.33, created on 2019-09-28 11:13:14
  from 'C:\xampp\htdocs\apirest\app\views\heroes\formHero.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d8f242ad3d690_66503605',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '134cb5c6ca75712337318740acb0f129f1fc71e6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\apirest\\app\\views\\heroes\\formHero.tpl',
      1 => 1569661923,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/nav.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_5d8f242ad3d690_66503605 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:template/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:template/nav.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="jumbotron">
  <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
</div>
<form>
  <div class="form-group">
    <label for="nameHero">Name</label>
    <input type="text" class="form-control" id="nameHero" placeholder="Type Name">
  </div>
  <div class="form-group">
    <label for="lastnameHero">Last Name</label>
    <input type="text" class="form-control" id="lastnameHero" placeholder="Type Last Name">
  </div>
  <div class="form-group">
    <label for="selectClass">Select Class</label>
    <select class="form-control" id="selectClass">
      <option>Paladin</option>
      <option>Range</option>
      <option>Barbarian</option>
      <option>Wizard</option>
      <option>Cleric</option>
      <option>Warrior</option>
      <option>Thief</option>
    </select>
  </div>
  <div class="form-group">
    <label for="selectRace">Select Race</label>
    <select class="form-control" id="selectRace">
      <option>Human</option>
      <option>Elf</option>
      <option>Halfling</option>
      <option>Dwarf</option>
      <option>Half-orc</option>
      <option>Half-elf</option>
      <option>Dragonborn</option>
    </select>
  </div>

  <div class="form-group">
    <label for="selectWeapon">Select Weapon</label>
    <select class="form-control" id="selectWeapon">
      <option>Sword</option>
      <option>Dagger</option>
      <option>Hammer</option>
      <option>Bow and Arrows</option>
      <option>Half-orc</option>
      <option>Half-elf</option>
      <option>Staff</option>
    </select>
  </div>
</form>
<?php $_smarty_tpl->_subTemplateRender('file:template/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
