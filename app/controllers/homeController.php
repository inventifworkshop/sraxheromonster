<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use SlimFacades\Container;
use SlimFacades\Model;
use SlimFacades\View;

    Class homeController {

        private $c;

        public function __construct($container) {
            $this->c = $container;
            Model::load('CBaseHero');
            Model::load('CBaseMonster');
        }

        public function home(Request $request, Response $response, $args)
        {
            $data = [
                'title' =>'Dashboard',
                'datah' => Container::get('CBaseHeroModel')->countAll(),
                'datam' => Container::get('CBaseMonsterModel')->countAll(),
            ];

            // die(var_dump($data));
            return View::render($response, 'index.tpl', $data);
        }

    }

/* path: ~app/controllers/homeController.php */