<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use SlimFacades\Container;
use SlimFacades\Model;
use SlimFacades\View;
use SlimFacades\Library;
use SlimFacades\Helper;

class heroController
{

    private $c;

    public function __construct($container)
    {
        $this->c = $container;
            # with facade
            Model::load('CBaseHero');
            # or you can use like this;
            ## $this->c->model->load('baseHero');
    }

    public function hero(Request $request, Response $response, $args)
    {
        $data = [
            'title' =>'Hero Dash Board',
        ];
        return View::render($response, 'heroes/hero.tpl', $data);
    }

    public function newHero(Request $request, Response $response, $args)
    {
        $data = [
            'title' =>'Hero Creation',
        ];
        return View::render($response, 'heroes/newHero.tpl', $data);
    }

    public function editHero(Request $request, Response $response, $args)
    {
        $result = Container::get('CBaseHeroModel')->getAll();
        $data = [
            'heroes' =>  json_decode(json_encode($result), true)
        ];
        return View::render($response, 'heroes/editHero.tpl', $data);
    }

    public function modifyHero(Request $request, Response $response, $args)
    {
        $result = Container::get('CBaseHeroModel')->getHeroById($request->getQueryParams());
        $data = json_decode(json_encode($result[0]), true);
        return View::render($response, 'heroes/modifyHero.tpl', $data);
    }
//////////////////////////////////////////////////////////////////////////////
// START SECTION: CRUD
//////////////////////////////////////////////////////////////////////////////
    //CREATE
    public function createHero(Request $request, Response $response, $args)
    {
        $result = Container::get('CBaseHeroModel')->create($request->getParsedBody());
        return $result;
    }
    //UPDATE
    public function updateByIdHero(Request $request, Response $response, $args)
    {
        $result = Container::get('CBaseHeroModel')->update($request->getParsedBody());
        exit($result);
    }
    //DELETE
    public function deleteHeroById(Request $request, Response $response, $args)
    {
        $params = $request->getQueryParams();
        $result = Container::get('CBaseHeroModel')->deleteById($params['id']);
        $result = str_replace("index.php","edithero",$result);
        header('Location: '. $result);
        exit();
    }
//////////////////////////////////////////////////////////////////////////////
// END SECTION: CRUD
//////////////////////////////////////////////////////////////////////////////

}

/* path: ~app/controllers/homeController.php */