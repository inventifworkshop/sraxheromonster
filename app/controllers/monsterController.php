<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use SlimFacades\Container;
use SlimFacades\Model;
use SlimFacades\View;
use SlimFacades\Library;
use SlimFacades\Helper;

class monsterController
{

    private $c;

    public function __construct($container)
    {
        $this->c = $container;
            # with facade
            Model::load('CBaseMonster');
    }

    public function monster(Request $request, Response $response, $args)
    {
        $data = [
            'title' =>'monster Dash Board',
        ];
        return View::render($response, 'monsters/monster.tpl', $data);
    }

    public function newMonster(Request $request, Response $response, $args)
    {
        $data = [
            'title' =>'Monster Creation',
        ];
        return View::render($response, 'monsters/newMonster.tpl', $data);
    }

    public function editMonster(Request $request, Response $response, $args)
    {
        $result = Container::get('CBaseMonsterModel')->getAll();
        $data = [
            'monsters' =>  json_decode(json_encode($result), true)
        ];
        return View::render($response, 'monsters/editMonster.tpl', $data);
    }

    public function modifyMonster(Request $request, Response $response, $args)
    {
        $result = Container::get('CBaseMonsterModel')->getMonsterById($request->getQueryParams());
        $data = json_decode(json_encode($result[0]), true);
        return View::render($response, 'monsters/modifyMonster.tpl', $data);
    }
//////////////////////////////////////////////////////////////////////////////
// START SECTION: CRUD
//////////////////////////////////////////////////////////////////////////////
    //CREATE
    public function createMonster(Request $request, Response $response, $args)
    {
        $result = Container::get('CBaseMonsterModel')->create($request->getParsedBody());
        exit($result);
    }
    //UPDATE
    public function updateByIdMonster(Request $request, Response $response, $args)
    {
        $result = Container::get('CBaseMonsterModel')->update($request->getParsedBody());
        exit($result);
    }
    //DELETE
    public function deleteMonsterById(Request $request, Response $response, $args)
    {
        $params = $request->getQueryParams();
        $result = Container::get('CBaseMonsterModel')->deleteById($params['id']);
        $result = str_replace("index.php","editmonster",$result);
        header('Location: '. $result);
        exit();
    }
//////////////////////////////////////////////////////////////////////////////
// END SECTION: CRUD
//////////////////////////////////////////////////////////////////////////////

}

/* path: ~app/controllers/homeController.php */