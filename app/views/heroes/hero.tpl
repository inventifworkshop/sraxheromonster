{include file='template/header.tpl'}

{include file='template/nav.tpl'}
<div class="jumbotron">
  <h1>{$title}</h1>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="card" style="width:250px">
    <img src={$smarty.const.DIR_IMAGES}{"heroes.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Create Hero</h5>
        <a href="newhero" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
    <div class="col-sm-6">
    <div class="card" style="width:250px">
    <img src={$smarty.const.DIR_IMAGES}{"edithero.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Edit Hero & Delete</h5>
        <a href="edithero" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
</div>
{include file='template/footer.tpl'}