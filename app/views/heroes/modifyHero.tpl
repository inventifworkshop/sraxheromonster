{assign var=racecompare value=["Human", "Elf", "Halfling", "Dwarf", "Half-orc", "Half-elf", "Dragonborn"]}
{assign var=classcompare value=["Paladin", "Ranger", "Barbarian", "Wizard", "Cleric", "Warrior", "Thief"]}
{assign var=weaponcompare value=["Sword", "Dagger", "Hammer", "Bow and Arrows", "Staff"]}
{include file='template/header.tpl'}
{include file='template/nav.tpl'}
<div class="jumbotron">
  <h1>Hero Modify</h1>
</div>

<form name="formModifyHero" id="formModifyHero" action={$SCRIPT_NAME|replace:'index.php':'updatehero'} method="post">
  <div class="form-group">
    <label for="newHeroFirstName">Name Hero</label>
    <input type="text" class="form-control" id="newHeroFirstName" name="name" required value={$name}>
  </div>
  <div class="form-group">
    <label for="newHeroLastName">Last Name</label>
    <input type="text" class="form-control" id="newHeroLastName" name="last_name" value={$last_name}>
  </div>
  <div class="form-group">
    <label for="selectRace">Select Race {$race}</label>
    <select class="form-control" id="selectRace" name="race" value={$race}>
    {foreach from=$racecompare item=key}
    {if $key eq {$race}}
	    <option value={$key} selected>{$key}</option>
    {else}
      <option value={$key}>{$key}</option>
    {/if}
    {/foreach}
    </select>
  </div>
  <div class="form-group">
    <label for="selectClass">Select Class</label>
    <select class="form-control" id="selectClass" name="class">
    {foreach from=$classcompare item=key}
    {if $key eq {$class}}
	    <option value={$key} selected>{$key}</option>
    {else}
      <option value={$key}>{$key}</option>
    {/if}
    {/foreach}
    </select>
    </select>
  </div>
  <div class="form-group">
    <label for="selectWeapon">Select Weapon</label>
    <select class="form-control" id="selectWeapon" name="weapon">
    {foreach from=$weaponcompare item=key}
    {if $key eq {$weapon}}
	    <option value={$key} selected>{$key}</option>
    {else}
      <option value={$key}>{$key}</option>
    {/if}
    {/foreach}
    </select>
  </div>
  <div class="form-group d-none">
    <label for="newHeroStrength">St Strength</label>
    <input type="text" class="form-control" id="newHeroStrength" name="st_strength ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroIntelligence">St Intelligence</label>
    <input type="text" class="form-control" id="newHeroIntelligence" name="st_intelligence ">
  </div>
  <div class="form-group d-none">
    <label for="newHeroDexterity">St Dexterity</label>
    <input type="text" class="form-control" id="newHeroDexterity" name="st_dexterity">
  </div>
  <div class="form-group d-none">
    <label for="modifyHeroId">ID</label>
    <input type="text" class="form-control" id="modifyHeroDexterity" name="id" value={$id}>
  </div>
  <button id="btnSubmitModifyHero" type="button" class="btn btn-primary">Submit</button>
</form>
{include file='template/footer.tpl'}