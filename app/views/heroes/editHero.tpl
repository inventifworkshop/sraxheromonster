{include file='template/header.tpl'}

{include file='template/nav.tpl'}
<div class="jumbotron">
  <h1>Hero Edit</h1>
</div>
<div class="row">
<div class="col-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Number</th>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Race</th>
            <th scope="col">Class</th>
            <th scope="col">Weapon</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        {foreach from=$heroes item=heroe name=i}
          <tr>
            <th scope="row">{$smarty.foreach.i.iteration}</th>
            <td>{$heroe['name']}</td>
            <td>{$heroe['last_name']}</td>
            <td>{$heroe['race']}</td>
            <td>{$heroe['class']}</td>
            <td>{$heroe['weapon']}</td>
            <td>
              <button type="button" class="btn btn-success"><a style="color: white;" href="modifyhero?id={$heroe['id']}">Update</a></button>
            <button type="button" class="btn btn-danger"><a style="color: white;" href="deletehero?id={$heroe['id']}">Delete</a></button>
            </td>
          </tr>
          {/foreach}
        </tbody>
      </table>
    </div>
</div>
{include file='template/footer.tpl'}