{include file='template/header.tpl'}

{include file='template/nav.tpl'}
<div class="jumbotron">
  <h1>{$title}</h1>
</div>
<form name="formNewHero" id="formNewHero" action={$SCRIPT_NAME|replace:'index.php':'createhero'} method="post">
  <div class="form-group">
    <label for="newHeroFirstName">Name Hero</label>
    <input type="text" class="form-control" id="newHeroFirstName" name="name" required>
  </div>
  <div class="form-group">
    <label for="newHeroLastName">Last Name</label>
    <input type="text" class="form-control" id="newHeroLastName" name="last_name">
  </div>
  <div class="form-group">
    <label for="selectRace">Select Race</label>
    <select class="form-control" id="selectRace" name="race">
      <option value="Human">Human</option>
      <option value="Elf">Elf</option>
      <option value="Halfling">Halfling</option>
      <option value="Dwarf">Dwarf</option>
      <option value="Half-orc">Half-orc</option>
      <option value="Half-elf">Half-elf</option>
      <option value="Dragonborn">Dragonborn</option>
    </select>
  </div>
  <div class="form-group">
    <label for="selectClass">Select Class</label>
    <select class="form-control" id="selectClass" name="class">
      <option value="Paladin">Paladin</option>
      <option value="Ranger" disabled>Ranger</option>
      <option value="Barbarian" disabled>Barbarian</option>
      <option value="Wizard" disabled>Wizard</option>
      <option value="Cleric"disabled>Cleric</option>
      <option value="Warrior" disabled>Warrior</option>
      <option value="Thief" disabled>Thief</option>
    </select>
  </div>
  <div class="form-group">
    <label for="selectWeapon">Select Weapon</label>
    <select class="form-control" id="selectWeapon" name="weapon">
      <option value="Sword">Sword</option>
      <option value="Dagger">Dagger</option>
      <option value="Hammer" disabled>Hammer</option>
      <option value="Bow and Arrows" disabled>Bow and Arrows</option>
      <option value="Staff" disabled>Staff</option>
    </select>
  </div>
  <div class="form-group d-none">
    <label for="newHeroStrength">St Strength</label>
    <input type="text" class="form-control" id="newHeroStrength" name="st_strength ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroIntelligence">St Intelligence</label>
    <input type="text" class="form-control" id="newHeroIntelligence" name="st_intelligence ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroDexterity">St Dexterity</label>
    <input type="text" class="form-control" id="newHeroDexterity" name="st_dexterity">
  </div>
  <button id="btnSubmitNewHero" type="button" class="btn btn-primary">Submit</button>
  <button id="newHeroButtonRandomize" class="btn btn-primary">Randomize</button>
  <button id="newHeroButtonClearForm" class="btn btn-primary">Clear form</button>
</form>
{include file='template/footer.tpl'}