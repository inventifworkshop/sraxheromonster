{include file='template/header.tpl'}
{include file='template/nav.tpl'}
<div class="jumbotron">
  <h1>{$title}</h1>
</div>
<div class="row">
  <div class="col-sm-3">
    <div class="card" style="width:200px">
    <img src={$smarty.const.DIR_IMAGES}{"heroes.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Total Available Heroes</h5>
        <h3>{$datah['totalheroes']}</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card" style="width:200px">
    <img src={$smarty.const.DIR_IMAGES}{"popularhero.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Hero Race</h5>
        <h3>{$datah['race']}</h3>
      </div>
    </div>
  </div>
    <div class="col-sm-3">
    <div class="card" style="width:200px">
    <img src={$smarty.const.DIR_IMAGES}{"popularclass.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Hero Class</h5>
        <h3>{$datah['class']}</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="card" style="width:200px">
    <img src={$smarty.const.DIR_IMAGES}{"popularweapon.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Weapon</h5>
        <h3>{$datah['weapon']}</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card" style="width:200px">
    <img src={$smarty.const.DIR_IMAGES}{"monsters.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Total Available Monsters</h5>
        <h3>{$datam['totalmonsters']}</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card" style="width:200px">
    <img src={$smarty.const.DIR_IMAGES}{"popularrace.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Race</h5>
        <h3>{$datam['race']}</h3>
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card" style="width:200px">
    <img src={$smarty.const.DIR_IMAGES}{"popularability.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Popular Ability</h5>
        <h3>{$datam['power']}</h3>
      </div>
    </div>
  </div>
</div>
{include file='template/footer.tpl'}