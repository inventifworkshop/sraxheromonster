{assign var=racearray value=["Beholder","Mind Flayer","Drow","Dragons","Owlbear","Bulette","Rust Monster","Gelatinous
Cube","Hill Giant","Stone Giant","Frost Giant","Fire Giant","Cloud Giant","Storm Giant","
Displacer Beast","Githyanki","Kobold","Kuo-Toa","Lich","Orc","Slaad","Umber Hulk","Yuan-ti"]}
{assign var=powerarray value=["Shadow Ball"=> 0,"Aerial Ace"=> 0,"Giga Drain"=> 0,"Thunderbolt"=> 0,"Earthquake"=> 0,"Crunch"=> 0,"Double Team"=> 0,"
Psychic"=> 0,"Ice Beam"=> 0,"Surf"=> 0]}

{include file='template/header.tpl'}
{include file='template/nav.tpl'}
<div class="jumbotron">
  <h1>Monster Modify</h1>
</div>

<form name="formModifyMonster" id="formModifyMonster" action={$SCRIPT_NAME|replace:'index.php':'updatemonster'} method="post">
  <div class="form-group">
    <label for="newHeroFirstName">Name Hero</label>
    <input type="text" class="form-control" id="newHeroFirstName" name="name" required value={$name}>
  </div>
  <div class="form-group">
    <label for="newHeroLastName">Last Name</label>
    <input type="text" class="form-control" id="newHeroLastName" name="last_name" value={$last_name}>
  </div>
  <div class="form-group">
    <label for="selectRace">Select Race {$race}</label>
    <select class="form-control" id="selectRace" name="race" value={$race}>
    {foreach from=$racearray item=key}
    {if $key eq {$race}}
	    <option value={$key} selected>{$key}</option>
    {else}
      <option value={$key}>{$key}</option>
    {/if}
    {/foreach}
    </select>
  </div>
    <div class="form-group">
    <label for="selectPower">Select Power {$power}</label>
    <select class="form-control" id="selectPower" name="power" value={$race}>
    {foreach from=$powerarray item=key}
    {if $key eq {$power}}
	    <option value={$key} selected>{$key}</option>
    {else}
      <option value={$key}>{$key}</option>
    {/if}
    {/foreach}
    </select>
  </div>
  <div class="form-group d-none">
    <label for="newHeroStrength">St Strength</label>
    <input type="text" class="form-control" id="newHeroStrength" name="st_strength ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroIntelligence">St Intelligence</label>
    <input type="text" class="form-control" id="newHeroIntelligence" name="st_intelligence ">
  </div>
  <div class="form-group d-none">
    <label for="newHeroDexterity">St Dexterity</label>
    <input type="text" class="form-control" id="newHeroDexterity" name="st_dexterity">
  </div>
  <div class="form-group d-none">
    <label for="modifyMonsterId">ID</label>
    <input type="text" class="form-control" id="modifyMonsterId" name="id" value={$id}>
  </div>
  <button id="btnSubmitModifyMonster" type="button" class="btn btn-primary">Submit</button>
</form>
{include file='template/footer.tpl'}