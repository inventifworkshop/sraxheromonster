{include file='template/header.tpl'}

{include file='template/nav.tpl'}
<div class="jumbotron">
  <h1>Monster Edit</h1>
</div>
<div class="row">
<div class="col-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Number</th>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Race</th>
            <th scope="col">Power</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        {foreach from=$monsters item=monster name=i}
          <tr>
            <th scope="row">{$smarty.foreach.i.iteration}</th>
            <td>{$monster['name']}</td>
            <td>{$monster['last_name']}</td>
            <td>{$monster['race']}</td>
            <td>{$monster['power']}</td>
            <td>
              <button type="button" class="btn btn-success"><a style="color: white;" href="modifymonster?id={$monster['id']}">Update</a></button>
            <button type="button" class="btn btn-danger"><a style="color: white;" href="deletemonster?id={$monster['id']}">Delete</a></button>
            </td>
          </tr>
          {/foreach}
        </tbody>
      </table>
    </div>
</div>
{include file='template/footer.tpl'}