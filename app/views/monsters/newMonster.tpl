{assign var=racearray value=["Beholder","Mind Flayer","Drow","Dragons","Owlbear","Bulette","Rust Monster","Gelatinous
Cube","Hill Giant","Stone Giant","Frost Giant","Fire Giant","Cloud Giant","Storm Giant","
Displacer Beast","Githyanki","Kobold","Kuo-Toa","Lich","Orc","Slaad","Umber Hulk","Yuan-ti"]}
{assign var=powerarray value=["Shadow Ball","Aerial Ace","Giga Drain","Thunderbolt","Earthquake","Crunch","Double Team","
Psychic","Ice Beam","Surf"]}

{include file='template/header.tpl'}
{include file='template/nav.tpl'}
<div class="jumbotron">
  <h1>{$title}</h1>
</div>
<form name="formNewMonster" id="formNewMonster" action={$SCRIPT_NAME|replace:'index.php':'createmonster'} method="post">
  <div class="form-group">
    <label for="newMonsterFirstName">Name Monster</label>
    <input type="text" class="form-control" id="newMonsterFirstName" name="name" required>
  </div>
  <div class="form-group">
    <label for="newMonsterLastName">Last Monster</label>
    <input type="text" class="form-control" id="newMonsterLastName" name="last_name">
  </div>
  <div class="form-group">
    <label for="selectRace">Select Race</label>
    <select class="form-control" id="monsterSelectRace" name="race">
    {foreach from=$racearray item=key}
      <option value="{$key}">{$key}</option>
    {/foreach}
    </select>
  </div>
  <div class="form-group">
    <label for="selectPower">Select Power</label>
    <select class="form-control" id="MonsterSelectPower" name="power">
    {foreach from=$powerarray item=key}
      <option value="{$key}">{$key}</option>
    {/foreach}
    </select>
  </div>
  <div class="form-group d-none">
    <label for="newHeroStrength">St Strength</label>
    <input type="text" class="form-control" id="newHeroStrength" name="st_strength ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroIntelligence">St Intelligence</label>
    <input type="text" class="form-control" id="newHeroIntelligence" name="st_intelligence ">
  </div>
    <div class="form-group d-none">
    <label for="newHeroDexterity">St Dexterity</label>
    <input type="text" class="form-control" id="newHeroDexterity" name="st_dexterity">
  </div>
  <button id="btnSubmitNewMonster" type="button" class="btn btn-primary">Submit</button>
  <button id="newHeroButtonRandomize" class="btn btn-primary">Randomize</button>
  <button id="newHeroButtonClearForm" class="btn btn-primary">Clear form</button>
</form>
{include file='template/footer.tpl'}