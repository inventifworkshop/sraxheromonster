{include file='template/header.tpl'}

{include file='template/nav.tpl'}
<div class="jumbotron">
  <h1>{$title}</h1>
</div>
<div class="row">
  <div class="col-sm-4">
    <div class="card" style="width:250px">
    <img src={$smarty.const.DIR_IMAGES}{"monsters.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Create Monster</h5>
        <a href="newmonster" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
    <div class="col-sm-4">
    <div class="card" style="width:250px">
    <img src={$smarty.const.DIR_IMAGES}{"editmonster.png"} class="card-img-top" alt="icon image">
      <div class="card-body">
        <h5 class="card-title">Edit Monster</h5>
        <a href="editmonster" class="btn btn-primary">Go</a>
      </div>
    </div>
  </div>
</div>
{include file='template/footer.tpl'}