<!doctype html>
<html lang="en" class="h-100">
<head>
    <title>SlimMVC</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="nofollow,noindex">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600&amp;subset=latin-ext" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="public/assets/v1/js/utils.js"></script>
</head>
<body class="d-flex flex-column h-100">
<div class="container">