<?php

$configuration['settings']['database']['default'] = [
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'heroesandmonsters',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix' => '',
];

/* path: ~app/config/database.php */