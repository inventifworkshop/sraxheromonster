<?php
use SlimFacades\Route;

//////////////////////////////////////////////////////////////////////////////
// START SECTION: DASHBOARD ROUTE
///////////////////////////////////////////////////////////////////////////////
Route::get('/', '\homeController:home')
     ->setName('homePage');
Route::get('/home', '\homeController:home')
     ->setName('homePage');

///////////////////////////////////////////////////////////////////////////////
// END SECTION: DASHBOARD ROUTE
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// START SECTION: HEROES ROUTE
///////////////////////////////////////////////////////////////////////////////
Route::get('/hero', '\heroController:hero')
     ->setName('heroPage')->add(new heroMiddleware());

Route::get('/newhero', '\heroController:newHero')
     ->setName('newHeroPage');

Route::get('/edithero', '\heroController:editHero')
     ->setName('editHeroPage');

Route::get('/modifyhero', '\heroController:modifyHero')
     ->setName('modifyHeroPage');     

Route::post('/createhero', '\heroController:createHero')
     ->setName('createHeroPage');

Route::post('/updatehero', '\heroController:updateByIdHero')
    ->setName('updateHeroPage');

    Route::get('/deletehero', '\heroController:deleteHeroById')
    ->setName('deleteHeroByIdPage');

///////////////////////////////////////////////////////////////////////////////
// END SECTION: HEROES ROUTE
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// START SECTION: MONSTERS ROUTE
///////////////////////////////////////////////////////////////////////////////

Route::get('/monster', '\monsterController:monster')
     ->setName('monsterPage');

Route::get('/newmonster', '\monsterController:newMonster')
     ->setName('newmonsterPage');

Route::get('/editmonster', '\monsterController:editMonster')
     ->setName('editmonsterPage');

Route::get('/modifymonster', '\monsterController:modifyMonster')
     ->setName('modifyMonsterPage');     

Route::post('/createmonster', '\monsterController:createMonster')
     ->setName('createMonsterPage');

Route::post('/updatemonster', '\monsterController:updateByIdMonster')
    ->setName('updateByIdMonsterMonsterPage');

    Route::get('/deletemonster', '\monsterController:deleteMonsterById')
    ->setName('deleteMonsterByIdPage');     

///////////////////////////////////////////////////////////////////////////////
// END SECTION: MONSTERS ROUTE
///////////////////////////////////////////////////////////////////////////////

Route::get('/view-test', '\sampleController:viewTest')
    ->setName('viewTestPage');

Route::get('/schema-test', '\sampleController:schemaTest')
    ->setName('schemaTestPage');

Route::get('/model-test', '\sampleController:modelTest')
    ->setName('modelTestPage');

Route::get('/helper-test', '\sampleController:helperTest')
    ->setName('helperTestPage');

Route::get('/library-test', '\sampleController:libraryTest')
    ->setName('libraryTestPage');

    Route::get('/sample-page', '\sampleController:sampleFunction')
	->setName('SampleFunctionHeroPage')->add( new sampleMiddleware() );

/* path: ~app/config/routes.php */