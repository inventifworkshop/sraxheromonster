var FirstNames = ["Bheizer", "Khazun", "Grirgel", "Murgil", "Edraf", "En", "Grognur", "Grum", "Surhathion", "Lamos", "Melmedjad", "Shouthes", "Che", "Jun", "Rircurtun", "Zelen"];
var LastNames = ["Nema", "Dhusher", "Burningsun", "Hawkglow", "Nav", "Kadev", "Lightkeeper", "Heartdancer", "Fivrithrit", "Suechit", "Tuldethatvo", "Vrovakya", "Hiao", "Chiay", "Hogoscu", "Vedrimor"];
var Classes = ["Paladin", "Ranger", "Barbarian", "Wizard", "Cleric", "Warrior", "Thief"];
var Races = ["Human", "Elf", "Halfling", "Dwarf", "Half-orc", "Half-elf", "Dragonborn"];
var Weapons = ["Sword", "Dagger", "Hammer", "Bow and Arrows", "Staff"];

MonsterRaces = ["Beholder", "Mind Flayer", "Drow", "Dragons", "Owlbear", "Bulette", "Rust Monster", "Gelatinous Cube", "Hill Giant", "Stone Giant", "Frost Giant", "Fire Giant", "Cloud Giant", "Storm Giant", "Displacer Beast", "Githyanki", "Kobold", "Kuo-Toa", "Lich", "Orc", "Slaad", "Umber Hulk", "Yuan-ti"];
MonsterPowers = ["Shadow Ball", "Aerial Ace", "Giga Drain", "Thunderbolt", "Earthquake", "Crunch", "Double Team", "Psychic", "Ice Beam", "Surf"];

$(document).ready(function(){

    $("#newHeroButtonRandomize").click(function (e) {
        e.preventDefault();
        DisableFormFields();
        GenerateRandomHero();
    });

    $("#btnSubmitNewHero").click(function(){
        createHero();
    });

    $("#btnSubmitNewMonster").click(function(){
        createMonster();
    });

    $("#btnSubmitModifyHero").click(function(){
        updateHero();
    });

    $("#btnSubmitModifyMonster").click(function(){
        updateMonster();
    });
    
    $("#newHeroButtonClearForm").click(function (e) {
        e.preventDefault();
        EnableFormFields();
        document.getElementById("newHeroFirstName").value = "";
        document.getElementById("newHeroLastName").value = "";
        $("#selectRace").val("Human").trigger('change');
    });

    $("#selectRace").change(function(){
        UpdateSelectClass(this.value);  
    });

    $("#selectClass").change(function(){
        UpdateSelectWeapon(this.value);
    });

    $("#monsterSelectRace").change(function(){
        UpdateMonsterSelectClass(this.value);  
    });
});

function DisableFormFields()
{
    $("#newHeroFirstName").prop('disabled', true);
    $("#newHeroLastName").prop('disabled', true);
    $("#selectClass").prop('disabled', true);
    $("#selectRace").prop('disabled', true);
    $("#selectWeapon").prop('disabled', true);
}

function EnableFormFields()
{
    $("#newHeroFirstName").prop('disabled', false);
    $("#newHeroLastName").prop('disabled', false);
    $("#selectClass").prop('disabled', false);
    $("#selectRace").prop('disabled', false);
    $("#selectWeapon").prop('disabled', false);
}

function GenerateRandomHero()
{
    var random_race = Races[Math.floor(Math.random() * Races.length)];
    var random_name = GenerateSpecificRandomName(random_race);
    var random_class = GenerateSpecificRandomClass(random_race);
    var random_weapon = GenerateSpecificRandomWeapon(random_class);
    var first_name = GetFirstName(random_name);
    var last_name = GetLastName(random_name);

    document.getElementById("newHeroFirstName").value = first_name;
    if(!first_name !== "")
    {
        document.getElementById("newHeroLastName").value = last_name;
    }
    document.getElementById("selectRace").value = random_race;
    document.getElementById("selectClass").value = random_class;
    document.getElementById("selectWeapon").value = random_weapon;
}

function GenerateSpecificRandomName(race)
{
    var fullname = "";
	switch(race)
	{
		case "Human":
        case "Half-elf":
        case "Halfling":
			var first_name = GenerateRandomFirstName();
			var last_name = GenerateRandomLastName();
            fullname = first_name + " " + last_name;
		break;
		case "Elf":
            var first_name = GenerateRandomFirstName();
            var last_name = first_name;
            last_name = last_name.charAt(0).toLowerCase() + last_name.slice(1);
            last_name = last_name.split("").reverse().join("");
            last_name = last_name.charAt(0).toUpperCase() + last_name.slice(1);
            fullname = first_name + " " + last_name;
		break;
		case "Dwarf":
            var first_name = "";
            var last_name = "";
            var bad_name = true;
			do {
                first_name = GenerateRandomFirstName();
                fn_copy = first_name.toLowerCase();
                if(fn_copy.includes("r") || fn_copy.includes("h"))
                {
                    bad_name = false;
                }
                else
                {
                    bad_name = true;
                }
                
            } while (bad_name);

            bad_name = true;

            do {
                last_name = GenerateRandomLastName();
                ln_copy = last_name.toLowerCase();
                if(ln_copy.includes("r") || ln_copy.includes("h"))
                {
                    bad_name = false;
                }
                else
                {
                    bad_name = true;
                }
                
            } while (bad_name);

            fullname = first_name + " " + last_name;
		break;
		case "Half-orc":
        case "Dragonborn":
			fullname = GenerateRandomFirstName();
		break;
    }
    return fullname;
}

function GenerateSpecificRandomClass(race)
{
    var hero_class = "";
	switch(race)
	{
		case "Human":
        case "Half-elf":
			hero_class = "Paladin";
		break;
		case "Elf":
            var bad_class = true;
			do {
                random_class = GenerateRandomClass();
                rc_copy = random_class.toLowerCase();
                if(!fn_copy.includes("barbarian") && !fn_copy.includes("warrior"))
                {
                    bad_class = false;
                }
                else
                {
                    bad_class = true;
                }
                
            } while (bad_class);

            hero_class = random_class;
		break;
		case "Halfling":
                var bad_class = true;
                do {
                    random_class = GenerateRandomClass();
                    rc_copy = random_class.toLowerCase();
                    if(!rc_copy.includes("barbarian"))
                    {
                        bad_class = false;
                    }
                    else
                    {
                        bad_class = true;
                    }
                    
                } while (bad_class);
    
                hero_class = random_class;
		break;
		case "Dwarf":
            var bad_class = true;
			do {
                random_class = GenerateRandomClass();
                rc_copy = random_class.toLowerCase();
                if(!rc_copy.includes("ranger") && !rc_copy.includes("wizard"))
                {
                    bad_class = false;
                }
                else
                {
                    bad_class = true;
                }
                
            } while (bad_class);

            hero_class = random_class;
		break;
		case "Half-orc":
                var bad_class = true;
                do {
                    random_class = GenerateRandomClass();
                    rc_copy = random_class.toLowerCase();
                    if(!rc_copy.includes("wizard") && !rc_copy.includes("cleric"))
                    {
                        bad_class = false;
                    }
                    else
                    {
                        bad_class = true;
                    }
                    
                } while (bad_class);
    
                hero_class = random_class;
		break;
		case "Dragonborn":
                var bad_class = true;
                do {
                    random_class = GenerateRandomClass();
                    rc_copy = random_class.toLowerCase();
                    if(!rc_copy.includes("cleric"))
                    {
                        bad_class = false;
                    }
                    else
                    {
                        bad_class = true;
                    }
                    
                } while (bad_class);
    
                hero_class = random_class;
		break;
    }
    return hero_class;
}

function GenerateSpecificRandomWeapon(heroClass)
{
    var hero_weapon = "";
    switch(heroClass)
    {
        case "Paladin":
                var bad_weapon = true;
                do {
                    random_weapon = GenerateRandomWeapon();
                    rw_copy = random_weapon.toLowerCase();
                    if(rw_copy.includes("sword") || rw_copy.includes("dagger"))
                    {
                        bad_weapon = false;
                    }
                    else
                    {
                        bad_weapon = true;
                    }
                    
                } while (bad_weapon);
    
                hero_weapon = random_weapon;
        break;
        case "Ranger":
                var bad_weapon = true;
                do {
                    random_weapon = GenerateRandomWeapon();
                    rw_copy = random_weapon.toLowerCase();
                    if(rw_copy.includes("bow") || rw_copy.includes("dagger"))
                    {
                        bad_weapon = false;
                    }
                    else
                    {
                        bad_weapon = true;
                    }
                    
                } while (bad_weapon);
    
                hero_weapon = random_weapon;
        break;
        case "Barbarian":
                var bad_weapon = true;
                do {
                    random_weapon = GenerateRandomWeapon();
                    rw_copy = random_weapon.toLowerCase();
                    if(!rw_copy.includes("bow") && !rw_copy.includes("staff"))
                    {
                        bad_weapon = false;
                    }
                    else
                    {
                        bad_weapon = true;
                    }
                    
                } while (bad_weapon);
    
                hero_weapon = random_weapon;
        break;
        case "Wizard":
        case "Cleric":
                var bad_weapon = true;
                do {
                    random_weapon = GenerateRandomWeapon();
                    rw_copy = random_weapon.toLowerCase();
                    if(rw_copy.includes("staff") || rw_copy.includes("dagger"))
                    {
                        bad_weapon = false;
                    }
                    else
                    {
                        bad_weapon = true;
                    }
                    
                } while (bad_weapon);
    
                hero_weapon = random_weapon;
        break;
        case "Warrior":
                hero_weapon = GenerateRandomWeapon();
        break;
        case "Thief":
                var bad_weapon = true;
                do {
                    random_weapon = GenerateRandomWeapon();
                    rw_copy = random_weapon.toLowerCase();
                    if(!rw_copy.includes("hammer"))
                    {
                        bad_weapon = false;
                    }
                    else
                    {
                        bad_weapon = true;
                    }
                    
                } while (bad_weapon);
    
                hero_weapon = random_weapon;
        break;
    }
    return hero_weapon;
}

function UpdateSelectClass(newHeroRace)
{
    switch(newHeroRace)
    {
        case "Human":
        case "Half-elf":
            $("#selectClass").children().each(function(i, opt)
            {
                ov_copy = opt.value.toLowerCase();
                if(ov_copy.includes("paladin"))
                {
                    $(opt).prop('disabled', false);
                }
                else
                {
                    $(opt).prop('disabled', true);
                }

                if(document.getElementById("selectClass").value !== "Paladin")
                {
                    $("#selectClass").val("Paladin").trigger('change');
                }
            });
        break;
        case "Elf":
            $("#selectClass").children().each(function(i, opt)
            {
                ov_copy = opt.value.toLowerCase();
                if(ov_copy.includes("warrior") || ov_copy.includes("barbarian"))
                {
                    $(opt).prop('disabled', true);
                }
                else
                {
                    $(opt).prop('disabled', false);
                }

                if(document.getElementById("selectClass").value == "Warrior" || document.getElementById("selectClass").value == "Barbarian")
                {
                    $("#selectClass").val("Paladin").trigger('change');
                }
            });
        break;
        case "Halfling":
                $("#selectClass").children().each(function(i, opt)
                {
                    ov_copy = opt.value.toLowerCase();
                    if(ov_copy.includes("barbarian"))
                    {
                        $(opt).prop('disabled', true);
                    }
                    else
                    {
                        $(opt).prop('disabled', false);
                    }
    
                    if(document.getElementById("selectClass").value == "Barbarian")
                    {
                        $("#selectClass").val("Paladin").trigger('change');
                    }
                });
        break;
        case "Dwarf":
                $("#selectClass").children().each(function(i, opt)
                {
                    ov_copy = opt.value.toLowerCase();
                    if(ov_copy.includes("ranger") || ov_copy.includes("wizard"))
                    {
                        $(opt).prop('disabled', true);
                    }
                    else
                    {
                        $(opt).prop('disabled', false);
                    }
    
                    if(document.getElementById("selectClass").value == "Ranger" || document.getElementById("selectClass").value == "Wizard")
                    {
                        $("#selectClass").val("Paladin").trigger('change');
                    }
                });
        break;
        case "Half-orc":
                $("#selectClass").children().each(function(i, opt)
                {
                    ov_copy = opt.value.toLowerCase();
                    if(ov_copy.includes("cleric") || ov_copy.includes("wizard"))
                    {
                        $(opt).prop('disabled', true);
                    }
                    else
                    {
                        $(opt).prop('disabled', false);
                    }
    
                    if(document.getElementById("selectClass").value == "Cleric" || document.getElementById("selectClass").value == "Wizard")
                    {
                        $("#selectClass").val("Paladin").trigger('change');
                    }
                });
        break;
        case "Dragonborn":
                $("#selectClass").children().each(function(i, opt)
                {
                    ov_copy = opt.value.toLowerCase();
                    if(ov_copy.includes("cleric"))
                    {
                        $(opt).prop('disabled', true);
                    }
                    else
                    {
                        $(opt).prop('disabled', false);
                    }
    
                    if(document.getElementById("selectClass").value == "Cleric")
                    {
                        $("#selectClass").val("Paladin").trigger('change');
                    }
                });
        break;
    }
}

function UpdateSelectWeapon(newHeroClass)
{
    switch(newHeroClass)
    {
        case "Paladin":
                $("#selectWeapon").children().each(function(i, opt)
                {
                    ov_copy = opt.value.toLowerCase();
                    if(ov_copy.includes("sword") || ov_copy.includes("dagger"))
                    {
                        $(opt).prop('disabled', false);
                    }
                    else
                    {
                        $(opt).prop('disabled', true);
                    }
    
                    if(document.getElementById("selectWeapon").value !== "Sword" )
                    {
                        if(document.getElementById("selectWeapon").value !== "Dagger" )
                        {
                            $("#selectWeapon").val("Sword").trigger('change');
                        }
                    }
                });
        break;
        case "Ranger":
                $("#selectWeapon").children().each(function(i, opt)
                {
                    ov_copy = opt.value.toLowerCase();
                    if(ov_copy.includes("bow") || ov_copy.includes("dagger"))
                    {
                        $(opt).prop('disabled', false);
                    }
                    else
                    {
                        $(opt).prop('disabled', true);
                    }
    
                    if(document.getElementById("selectWeapon").value !== "Bow and Arrows" )
                    {
                        if(document.getElementById("selectWeapon").value !== "Dagger" )
                        {
                            $("#selectWeapon").val("Bow and Arrows").trigger('change');
                        }
                    }
                });
        break;
        case "Barbarian":
                $("#selectWeapon").children().each(function(i, opt)
                {
                    ov_copy = opt.value.toLowerCase();
                    if(ov_copy.includes("bow") || ov_copy.includes("staff"))
                    {
                        $(opt).prop('disabled', true);
                    }
                    else
                    {
                        $(opt).prop('disabled', false);
                    }
    
                    if(document.getElementById("selectWeapon").value == "Bow and Arrows" || document.getElementById("selectWeapon").value == "Staff")
                    {
                        $("#selectWeapon").val("Sword").trigger('change');
                    }
                });
        break;
        case "Wizard":
        case "Cleric":
                $("#selectWeapon").children().each(function(i, opt)
                {
                    ov_copy = opt.value.toLowerCase();
                    if(ov_copy.includes("staff") || ov_copy.includes("dagger"))
                    {
                        $(opt).prop('disabled', false);
                    }
                    else
                    {
                        $(opt).prop('disabled', true);
                    }
    
                    if(document.getElementById("selectWeapon").value !== "Staff" )
                    {
                        if(document.getElementById("selectWeapon").value !== "Dagger" )
                        {
                            $("#selectWeapon").val("Staff").trigger('change');
                        }
                    }
                });
        break;
        case "Warrior":
                $("#selectWeapon").children().each(function(i, opt)
            {
                $(opt).prop('disabled', false);
            });
        break;
        case "Thief":
                $("#selectWeapon").children().each(function(i, opt)
                {
                    ov_copy = opt.value.toLowerCase();
                    if(ov_copy.includes("hammer"))
                    {
                        $(opt).prop('disabled', true);
                    }
                    else
                    {
                        $(opt).prop('disabled', false);
                    }
    
                    if(document.getElementById("selectWeapon").value == "Hammer")
                    {
                        $("#selectWeapon").val("Sword").trigger('change');
                    }
                });
        break;
    }
}

function UpdateMonsterSelectClass(newMonsterRace)
{
    $("#MonsterSelectPower").children().each(function(i, opt)
    {
        switch(opt.value)
        {
            case "Shadow Ball":
                if(newMonsterRace == "Beholder" || newMonsterRace == "Mind Flayer")
                {
                    $(opt).prop('disabled', false);
                }
                else
                {
                    $(opt).prop('disabled', true);
                }
            break;
            case "Aerial Ace":
                if(newMonsterRace == "Beholder" || newMonsterRace == "Owlbear" || newMonsterRace == "Dragons" || newMonsterRace == "Cloud Giant" || newMonsterRace == "Storm Giant" || newMonsterRace == "Umber Hulk")
                {
                    $(opt).prop('disabled', false);
                }
                else
                {
                    $(opt).prop('disabled', true);
                }
            break;
            case "Surf":
                if(newMonsterRace == "Beholder" || newMonsterRace == "Yuan-ti" || newMonsterRace == "Gelatinous Cube" || newMonsterRace == "Drow")
                {
                    $(opt).prop('disabled', false);
                }
                else
                {
                    $(opt).prop('disabled', true);
                }
            break;
            case "Kobold":
                if(newMonsterRace == "Beholder" || newMonsterRace == "Double Team" || newMonsterRace == "Crunch")
                {
                    $(opt).prop('disabled', false);
                }
                else
                {
                    $(opt).prop('disabled', true);
                }
            break;
            case "Giga Drain":
                if(newMonsterRace == "Beholder" || newMonsterRace == "Mind Flyer")
                {
                    $(opt).prop('disabled', false);
                }
                else
                {
                    $(opt).prop('disabled', true);
                }
            break;
            default:
                    $(opt).prop('disabled', false);
            break;
        }
    });
}

function GenerateRandomFirstName()
{
 var first_name = FirstNames[Math.floor(Math.random() * FirstNames.length)];
 return first_name;
}

function GenerateRandomLastName()
{
    var last_name = LastNames[Math.floor(Math.random() * LastNames.length)];
    return last_name;
}

function GetFirstName(fullname)
{
    var split_name = fullname.split(" ");
    return split_name[0];
}

function GetLastName(fullname)
{
    var split_name = fullname.split(" ");
    if(split_name.length > 1)
    {
        return split_name[1];
    }
    else
    {
        return "";
    }
    
}

function GenerateRandomClass()
{
    var hero_class = Classes[Math.floor(Math.random() * Classes.length)];
    return hero_class;
}

function GenerateRandomWeapon()
{
    var weapon = Weapons[Math.floor(Math.random() * Weapons.length)];
    return weapon;
}

// sending data
function createHero(){
    var form = $('#formNewHero').serialize();
    
    $.ajax({
        url:"createhero",
        type: "POST",
        data: form
    })
    .done(function(response){
        console.log(response+"OK");
        alert("Saved hero");
    })
    .fail(function(xhr, error, request){
        console.log(xhr + ' ' + error + ' ' + request);
    });
}

function updateHero(){
    var form = $('#formModifyHero').serialize();
    
    $.ajax({
        url:"updatehero",
        type: "POST",
        data: form
    })
    .done(function(response){
        alert("Updated hero "+response);
    })
    .fail(function(xhr, error, request){
        console.log(xhr + ' ' + error + ' ' + request);
    });
}

// sending data
function createMonster(){
    var form = $('#formNewMonster').serialize();
    
    $.ajax({
        url:"createmonster",
        type: "POST",
        data: form
    })
    .done(function(response){
        console.log(response+"OK");
        alert("Saved monster");
    })
    .fail(function(xhr, error, request){
        console.log(xhr + ' ' + error + ' ' + request);
    });
}

function updateMonster(){
    var form = $('#formModifyMonster').serialize();
    
    $.ajax({
        url:"updatemonster",
        type: "POST",
        data: form
    })
    .done(function(response){
        alert("Updated monster "+response);
    })
    .fail(function(xhr, error, request){
        console.log(xhr + ' ' + error + ' ' + request);
    });
}